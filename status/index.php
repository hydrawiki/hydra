<?php
/**
 * Curse Inc.
 * Hydra Site Status Checks
 *
 * @author		Author: Alexia E. Smith
 * @copyright	(c) 2014 Curse Inc.
 * @link		http://www.curse.com
 */

define('MW_INSTALL_DIR', dirname(__DIR__));
define('STATUS_DIR', __DIR__);
define('SETTINGS_ONLY', true);

require_once(MW_INSTALL_DIR."/vendor/autoload.php");
require_once(MW_INSTALL_DIR."/includes/AutoLoader.php");
require_once(MW_INSTALL_DIR."/includes/Defines.php");

class siteChecks {
	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		if (strstr($_SERVER['HTTP_HOST'], '..') !== false) {
			exit;
		}

		//Save site name.
		if ($_SERVER['HTTP_HOST'] && file_exists(MW_INSTALL_DIR.'/sites/'.$_SERVER['HTTP_HOST'].'/LocalSettings.php')) {
			$this->site = $_SERVER['HTTP_HOST'];
		} else {
			$this->site = 'Hydra';
		}

		if (!defined('MOUSE_DIR')) {
			define('MOUSE_DIR', STATUS_DIR.'/mouse');
		}
		$settings['file'] = MW_INSTALL_DIR.'/LocalSettings.php';
		if (!class_exists('mouseHole')) {
			require_once(MOUSE_DIR.'/mouse.php');
		}

		//The mouseConfigMediawiki module has to load before any modules that require configuration data from the LocalSettings file.
		$this->mouse = mouseHole::instance(['config' => 'mouseConfigMediawiki'], $settings);
		mouseHole::$settings['DB']['use_database'] = false;
		$this->mouse->loadClasses(['output' => 'mouseOutputOutput', 'request' => 'mouseRequestHttp', 'DB' => 'mouseDatabaseMysqli', 'redis' => 'mouseCacheRedis', 'redisSession' => 'mouseCacheRedis', 'redisJobQueue' => 'mouseCacheRedis', 'memcache' => 'mouseCacheMemcache'], $settings);

		$this->dbName = mouseHole::$settings['misc']['dbName'];
		$this->searchServers = mouseHole::$settings['misc']['searchServers'];
		$this->parsoidUrl = mouseHole::$settings['misc']['parsoidUrl'];
		$this->lessoidUrl = mouseHole::$settings['misc']['lessoidUrl'];
	}

	/**
	 * Runs the Checks
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function run() {
		$this->status['database']		= $this->checkDatabase();
		$this->status['memcache']		= $this->checkMemcache();
		$this->status['redis_session']	= $this->checkRedisSession();
		$this->status['redis_cache']	= $this->checkRedisCache();
		$this->status['redis_jobqueue']	= $this->checkRedisJobQueue();
		$this->status['search']			= $this->checkSearchIndexes();
		$this->status['node']			= $this->checkNode();
		$this->status['environment']	= $this->checkEnvironment();
		$this->status['tmp']			= $this->checkTemporaryFolder();
	}

	/**
	 * Check Database
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkDatabase() {
		$message = [];
		$error = [];

		$failedConnect = false;
		try {
			$this->mouse->DB->init();
			$message[] = "Database connection initiated, database selected, and SET NAMES ran.";
		} catch (Exception $e) {
			$error[] = "Error:Failure - Failed to connect to database.";
			$this->mouse->output->sendHTTPStatus('503');
			$failedConnect = true;
		}

		if (!$failedConnect) {
			try {
				$this->mouse->DB->selectAndFetch(
					[
						'select'	=> 'u.*',
						'from'		=> ['user' => 'u'],
						'limit'		=> [0, 5]
					]
				);
				$message[] = "Selected members from database.";
			} catch (Exception $e) {
				$error[] = "Error:Failure - Failed to select members from database.";
				$this->mouse->output->sendHTTPStatus('503');
			}
		}

		return ['passed' => $message, 'error' => $error];
	}

	/**
	 * Check Memcache
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkMemcache() {
		$message = [];
		$error = [];

		$return = $this->mouse->memcache->init();
		if ($return) {
			$message[] = "Memcache connection initiated.";
		} else {
			$this->mouse->output->sendHTTPStatus('503');
			$error[] = "Error:Failure - Failed to connect to memcache.";
		}

		$key = 'siteChecks-'.gethostname();
		$return = $this->mouse->memcache->set($key, 'true');
		if ($return) {
			$message[] = "Set '{$key}' in memcache.";
		} else {
			$error[] = "Error:Warning - Failed to set to memcache.";
		}

		$return = $this->mouse->memcache->get($key);
		if ($return) {
			$message[] = "Retrieved '{$key}' from memcache.";
		} else {
			$error[] = "Error:Warning - Failed to retrieve from memcache.";
		}

		$return = $this->mouse->memcache->delete($key);
		if ($return) {
			$message[] = "Deleted '{$key}' from memcache.";
		} else {
			$error[] = "Error:Warning - Failed to delete from memcache.";
		}

		return ['passed' => $message, 'error' => $error];
	}

	/**
	 * Check Redis
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkRedisSession() {
		$message = [];
		$error = [];

		$return = $this->mouse->redisSession->init();
		if ($return) {
			$message[] = "Redis connected.";
		} else {
			$this->mouse->output->sendHTTPStatus('503');
			$error[] = "Error:Failure - Failed to initiate Redis.";
		}

		$key = 'sessionSiteChecks-'.gethostname();
		if ($this->mouse->redisSession->redisInitialized) {
			$return = $this->mouse->redisSession->set($key, true);
			if ($return) {
				$message[] = "Set '{$key}' in Redis.";
			} else {
				$error[] = "Error:Failure - Failed to set to Redis.";
			}

			$return = $this->mouse->redisSession->get($key);
			if ($return) {
				$message[] = "Retrieved '{$key}' from Redis.";
			} else {
				$error[] = "Error:Failure - Failed to retrieve from Redis.";
			}

			$return = $this->mouse->redisSession->del($key);
			if ($return) {
				$message[] = "Deleted '{$key}' from Redis.";
			} else {
				$error[] = "Error:Failure - Failed to delete from Redis.";
			}
		}

		return ['passed' => $message, 'error' => $error];
	}

	/**
	 * Check Redis
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkRedisCache() {
		$message = [];
		$error = [];

		$return = $this->mouse->redis->init();
		if ($return) {
			$message[] = "Redis connected.";
		} else {
			$this->mouse->output->sendHTTPStatus('503');
			$error[] = "Error:Failure - Failed to initiate Redis.";
		}

		$key = 'siteChecks-'.gethostname();
		if ($this->mouse->redis->redisInitialized) {
			$return = $this->mouse->redis->set($key, 'true');
			if ($return) {
				$message[] = "Set '{$key}' in Redis.";
			} else {
				$error[] = "Error:Failure - Failed to set to Redis.";
			}

			$return = $this->mouse->redis->get($key);
			if ($return) {
				$message[] = "Retrieved '{$key}' from Redis.";
			} else {
				$error[] = "Error:Failure - Failed to retrieve from Redis.";
			}

			$return = $this->mouse->redis->del($key);
			if ($return) {
				$message[] = "Deleted '{$key}' from Redis.";
			} else {
				$error[] = "Error:Failure - Failed to delete from Redis.";
			}
		}

		return ['passed' => $message, 'error' => $error];
	}

	/**
	 * Check Redis
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkRedisJobQueue() {
		$message = [];
		$error = [];

		$return = $this->mouse->redisJobQueue->init();
		if ($return) {
			$message[] = "Redis connected.";
		} else {
			$this->mouse->output->sendHTTPStatus('503');
			$error[] = "Error:Failure - Failed to initiate Redis.";
		}

		$key = 'siteChecks-'.gethostname();
		if ($this->mouse->redis->redisInitialized) {
			$return = $this->mouse->redisJobQueue->set($key, 'true');
			if ($return) {
				$message[] = "Set '{$key}' in Redis.";
			} else {
				$error[] = "Error:Failure - Failed to set to Redis.";
			}

			$return = $this->mouse->redisJobQueue->get($key);
			if ($return) {
				$message[] = "Retrieved '{$key}' from Redis.";
			} else {
				$error[] = "Error:Failure - Failed to retrieve from Redis.";
			}

			$return = $this->mouse->redisJobQueue->del($key);
			if ($return) {
				$message[] = "Deleted '{$key}' from Redis.";
			} else {
				$error[] = "Error:Failure - Failed to delete from Redis.";
			}
		}

		return ['passed' => $message, 'error' => $error];
	}

	/**
	 * Check Redis
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkSearchIndexes() {
		$message = [];
		$error = [];

		if (is_array($this->searchServers) && count($this->searchServers)) {
			foreach ($this->searchServers as $cluster => $servers) {
				foreach ($servers as $number => $server) {
					$host = $server['host'];
					$port = $server['port'];
					$output = [];
					$index = "{$this->dbName}_content";
					$url = "http://{$host}:{$port}/{$index}";
					exec("curl --max-time 2 -i ".escapeshellarg($url), $output);
					if (isset($output[0]) && strpos($output[0], 'HTTP/1.1 200 OK') !== false) {
						$message[] = "Index {$index} exists on {$url}";
					} else {
						$error[] = "Error:Failure - Failed to find index {$index} on {$url}";
					}

					$output = [];
					$index = "{$this->dbName}_general";
					$url = "http://{$host}:{$port}/{$index}";
					exec("curl --max-time 2 -i ".escapeshellarg($url), $output);
					if (isset($output[0]) && strpos($output[0], 'HTTP/1.1 200 OK') !== false) {
						$message[] = "Index {$index} exists on {$url}";
					} else {
						$error[] = "Error:Failure - Failed to find index {$index} on {$url}";
					}
				}
			}
		}

		return ['passed' => $message, 'error' => $error];
	}

	/**
	 * Check if node services are running.(Parsoid, LESSoid)
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkNode() {
		$message = [];
		$error = [];

		if ($this->parsoidUrl != false) {
			exec("curl -I --no-keepalive '{$this->parsoidUrl}'", $output);
			if (isset($output[0]) && strpos($output[0], 'HTTP/1.1 200 OK') !== false) {
				$message[] = "Parsoid is running on {$this->parsoidUrl}";
			} else {
				$this->mouse->output->sendHTTPStatus('503');
				$error[] = "Error:Failure - Parsoid is not running on {$this->parsoidUrl}";
			}
		}

		if ($this->lessoidUrl != false) {
			exec("curl -I --no-keepalive '{$this->lessoidUrl}'", $output);
			if (isset($output[0]) && strpos($output[0], 'HTTP/1.1 200 OK') !== false) {
				$message[] = "Lessoid is running on {$this->lessoidUrl}";
			} else {
				$this->mouse->output->sendHTTPStatus('503');
				$error[] = "Error:Failure - Lessoid is not running on {$this->lessoidUrl}";
			}
		}

		return ['passed' => $message, 'error' => $error];
	}

	/**
	 * Return environment information.
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkEnvironment() {
		$message[] = "ENV(NGINX)={$_SERVER['PHP_ENV']}";
		$message[] = "ENV(CMD)=".trim(exec('echo $PHP_ENV'));

		return ['passed' => $message, 'error' => []];
	}

	/**
	 * Return environment information.
	 *
	 * @access	public
	 * @return	array	Array of status messages.
	 */
	public function checkTemporaryFolder() {
		$tmpDir = array_map( 'getenv', [ 'TMPDIR', 'TMP', 'TEMP' ] );
		$tmpDir[] = sys_get_temp_dir();
		$tmpDir[] = ini_get( 'upload_tmp_dir' );
		$folder = false;
		foreach ($tmpDir as $tmp) {
			if ($tmp != '' && is_dir($tmp) && is_writable($tmp)) {
				$folder = $tmp;
				break;
			}
		}

		if (!$folder) {
			$this->mouse->output->sendHTTPStatus('503');
			return ['passed' => [], 'error' => ["Error:Failure - Is bad."]];
		}
		return ['passed' => ["Is good."], 'error' => []];
	}

	/**
	 * Send Output
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function sendOutput() {
		$this->sendHeader();

		if ($this->mouse->request->get['extra'] == 'true') {
			foreach ($this->status as $key => $data) {
				$this->send('<div class="check">');
				$this->send('<span class="section">'.ucfirst($key)."</span>\n");

				foreach ($data as $type => $messages) {
					if (count($messages)) {
						$this->send('....<span class="type">'.ucfirst($type)."</span>\n");

						foreach ($messages as $message) {
							if ($type == 'passed') {
								$this->send('........<span class="passed">'.$message."</span>\n");
							} else {
								$this->send('........<span class="error">'.$message."</span>\n");
							}
						}
					}
				}
				$this->send('</div>');
			}
		} else {
			$this->send('<table id="allChecks">');
			foreach ($this->status as $key => $data) {
				$this->send('<tr>');
				$this->send('<td>');
				$this->send('<span class="section">'.ucfirst($key)."</span>");
				$this->send('</td>');
				$this->send('<td>');
				if (count($data['error']) > 0 or count($data['passed']) == 0) {
					$this->send("<span class='error'>Error:Failure</span>");
				} else {
					$this->send("<span class='passed'>Passed</span>");
				}
				$this->send('</td>');
				$this->send('<tr>');
			}
			$this->send('</table>');
		}
		$this->sendFooter();
	}

	/**
	 * Sends the standard header.
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function sendHeader() {
		if (PHP_SAPI != 'cli') {
			$output = <<<HEADER
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>$this->site - Site Status Checks</title>
	<link rel="stylesheet" type="text/css" media='screen' href="static/global.css">
</head>
<body>
	<div id="site">
		<div id="header">
			<h1><a href="/">Curse</a></h1>
			<h2>$this->site - Site Status Checks</h2>
		</div>
HEADER;
			echo $output;
		}
	}

	/**
	 * Sends the standard footer.
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function sendFooter() {
		if (PHP_SAPI != 'cli') {
			$output = <<<FOOTER
	</div>
</body>
</html>
FOOTER;
			echo $output;
		}
	}

	/**
	 * Echo Helper
	 *
	 * @access	public
	 * @param	string	String to print.
	 * @return	void	[Outputs to screen]
	 */
	public function send($message) {
		if (PHP_SAPI == 'cli') {
			echo strip_tags($message);
		} else {
			echo '	'.nl2br($message);
		}
	}
}
$siteCheck = new siteChecks();
$siteCheck->run();
$siteCheck->sendOutput();
