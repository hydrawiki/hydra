<?php
/**
 * NoName Studios
 * Mouse Framework
 * Mouse Config Mediawiki - Converts a Mediawiki LocalSettings.php file into a mouse compatible configuration.
 *
 * @author 		Alexia E. Smith
 * @copyright	(c) 2010 - 2014 NoName Studios
 * @license		All Rights Reserved
 * @package		Mouse Framework
 * @link		http://www.nonamestudios.com/
 * @version		2.0
 *
**/

class mouseConfigMediawiki {
	/**
	 * Object Key
	 *
	 * @var		object
	 */
	public $objectKey;

	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	[Optional] Object key used to initialize the object to mouse.  Also serves as the settings array key.
	 * @return	void
	 */
	public function __construct($objectKey = 'mediawiki') {
		global $IP;

		//This Mediawiki configuration object is a prototype bridge between a Mediawiki LocalSettings.php file and the immature mouse configuration concept.
		$this->objectKey	= $objectKey;
		$this->settings		=& mouseHole::$settings[$this->objectKey];

		if (!defined('MEDIAWIKI')) {
			define('MEDIAWIKI', 'WTF');
		}
		if (!defined('SETTINGS_ONLY')) {
			define('SETTINGS_ONLY', 'WTF');
		}
		$IP = MW_INSTALL_DIR;
		require(mouseHole::$settings['file']);
		if (strpos(':', $wgDBserver) > 0) {
			list($server, $port) = explode(':', $wgDBserver, 2);
		} else {
			$server = $wgDBserver;
		}
		if (empty($port) && isset($wgDBport)) {
			$port = $wgDBport;
		} else {
			$port = 3306;
		}
		mouseHole::$settings['DB'] = array(
											'server'		=> $server,
											'port'			=> $port,
											'database'		=> $wgDBname,
											'user'			=> $wgDBuser,
											'pass'			=> $wgDBpassword,
											'use_database'	=> true
										);
		unset($server, $port);

		if ($wgMetaNamespace) {
			mouseHole::$settings['wiki']['wiki_name']		= $wgSitename;
			mouseHole::$settings['wiki']['wiki_domain']		= str_ireplace(array('http://', 'https://'), '', $wgServer);
			mouseHole::$settings['wiki']['wiki_meta_name']	= $wgMetaNamespace;
			mouseHole::$settings['wiki']['wiki_database']	= $wgDBname;
		} else {
			throw new Exception('MediaWiki Meta Name $wgMetaNamespace is not defined.  Class '.__CLASS__.' requires this to continue.');
		}

		if (count($wgMemCachedServers)) {
			list($server, $port) = explode(':', $wgMemCachedServers[0]);
			foreach ($wgMemCachedServers as $server) {
				if (is_string($server)) {
					list($host, $port) = explode(':', $server);
					mouseHole::$settings['memcache']['servers'][] = [
						'host'	=> $host,
						'port'	=> $port
					];
				} elseif (is_array($server)) {
					list($host, $port) = explode(':', $server[0]);
					mouseHole::$settings['memcache']['servers'][] = [
						'host'		=> $host,
						'port'		=> $port,
						'weight'	=> $server[1]
					];
				}
			}
			mouseHole::$settings['memcache']['use_memcache']	= true;
		}

		if (isset($wgRedisServers['cache'])) {
			(isset(mouseHole::$settings['redis']) && is_array(mouseHole::$settings['redis']['servers']) ? mouseHole::$settings['redis']['servers'] = array_merge(mouseHole::$settings['redis']['servers'], $wgRedisServers['cache']) : mouseHole::$settings['redis']['servers'] = $wgRedisServers['cache']);
			mouseHole::$settings['redis']['use_redis']	= true;
		}
		if (isset($wgRedisServers['session'])) {
			(isset(mouseHole::$settings['redisSession']) && is_array(mouseHole::$settings['redisSession']['servers']) ? mouseHole::$settings['redisSession']['servers'] = array_merge(mouseHole::$settings['redisSession']['servers'], $wgRedisServers['session']) : mouseHole::$settings['redisSession']['servers'] = $wgRedisServers['session']);
			mouseHole::$settings['redisSession']['use_redis']	= true;
		}
		if (isset($wgRedisServers['jobqueue'])) {
			(isset(mouseHole::$settings['redisJobQueue']) && is_array(mouseHole::$settings['redisJobQueue']['servers']) ? mouseHole::$settings['redisJobQueue']['servers'] = array_merge(mouseHole::$settings['redisJobQueue']['servers'], $wgRedisServers['jobqueue']) : mouseHole::$settings['redisJobQueue']['servers'] = $wgRedisServers['jobqueue']);
			mouseHole::$settings['redisJobQueue']['use_redis']	= true;
		}
		mouseHole::$settings['misc']['dbName'] = $wgDBname;
		mouseHole::$settings['misc']['searchServers'] = $wgCirrusSearchClusters;
		mouseHole::$settings['misc']['parsoidUrl'] = $wgVirtualRestConfig['modules']['parsoid']['url'];
		mouseHole::$settings['misc']['lessoidUrl'] = false;
	}
}
