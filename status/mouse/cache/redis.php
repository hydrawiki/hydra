<?php
/**
 * NoName Studios
 * Mouse Framework
 * Mouse Cache Redis - Interface to Redis, provides automatic connection setup.
 *
 * @author 		Alexia E. Smith
 * @copyright	(c) 2010 - 2014 NoName Studios
 * @license		All Rights Reserved
 * @package		Mouse Framework
 * @link		http://www.nonamestudios.com/
 * @version		2.0
 *
**/

class mouseCacheRedis {
	/**
	 * Redis Initialized Successfully
	 *
	 * @var		boolean
	 */
	public $redisInitialized = false;

	/**
	 * Object Key
	 *
	 * @var		object
	 */
	public $objectKey;

	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	[Optional] Object key used to initialize the object to mouse.  Also serves as the settings array key.
	 * @return	void
	 */
	public function __construct($objectKey = 'redis') {
		$this->objectKey	= $objectKey;
		$this->settings		=& mouseHole::$settings[$this->objectKey];

		//Automatic enable.
		if ($this->settings['use_redis']) {
			$this->enabled	= $this->init();
		} else {
			$this->enabled	= false;
		}
	}

	/**
	 * Magic call function to map $mouse->redis->command() calls to $mouse->redis->redis->command().
	 *
	 * @access	public
	 * @param	string	Called magic function name.
	 * @param	array	Array of arguments.
	 * @return	mixed
	 */
	public function __call($function, $arguments) {
		if ($this->redisInitialized) {
			try {
				return call_user_func_array([$this->redis, $function], $arguments);
			} catch (RedisException $e) {
				// attempt to re-establish a connection before trashing the redis object completely
				if (!$this->redis->isConnected() && $this->redis->connect($this->settings['servers']['host'], $this->settings['servers']['port'], 1) && !$this->redis->isConnected()) {
					$this->redisInitialized = false;
					$this->redis = null;
				}
			}
		}
	}

	/**
	 * Automatically initiate Redis connection.
	 *
	 * @access	public
	 * @return	void
	 */
	public function init() {
		if ($this->redisInitialized) {
			return $this->redisInitialized;
		}

		if ($this->settings['use_redis'] == 1 && isset($this->settings['servers'])) {
			if (class_exists('Redis')) {
				$this->redis = new Redis();

				try {
					//$this->settings['servers'], $options
					$this->redis->connect($this->settings['servers']['host'], $this->settings['servers']['port'], 1);
				} catch (RedisException $e) {
					var_dump($e);
					$this->redis = null;
				}

				if ($this->redis) {
					if (isset($this->settings['servers']['options']['readTimeout'])) {
						$this->redis->setOption(Redis::OPT_READ_TIMEOUT, $this->settings['servers']['options']['readTimeout']);
					}
					$this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_IGBINARY);
					$this->redisInitialized = true;
				}
			}
		}
		return $this->redisInitialized;
	}

	/**
	 * Initialize.
	 *
	 * @access	public
	 * @return	object
	 */
	static public function instance() {
		return new self();
	}
}
