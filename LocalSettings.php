<?php
/**
 *********************************************************
 * HYDRA DEVELOPERS - PLEASE READ
 *********************************************************
 *
 * Everything in the Hydra base LocalSettings.php is meant to bootstrap all the basics that all wikis use.
 *
 * Guidelines:
 *	$wgServer would not be defined here since it is wiki specific.
 *		Likewise anything that gets put in the generated in the sites/{domain}/LocalSettings.php files probably will not be defined here as well.
 *			Use your brain on this one.  Rarely there are some settings that just have to be defined earlier in the settings stack.
 *
 *	$wgDefaultSkin would would be defined here since it is the same across all wikis.
 *
 *	If it is defined in Special:WikiAllowedSettings then it should never show up here.
 *		Q: What if I need it on the master wiki?  A: Put in master/LocalSettings.php.
 *
 *	Extension specific settings should either be:
 *		Defined in Special:WikiAllowedSettings for extensions configued in Special:WikiAllowedExtensions.
 *		Defined in Extensions.php for hard coded extensions loaded in there.
 *
 */

// Protect against web entry
if (!defined('MEDIAWIKI')) {
	exit;
}

require $IP . '/settings/Secrets.php'; // Avoid storing secrets/passwords in this file.

$bareDomain = $secrets['HYDRA_BARE_DOMAIN'];
if (empty($bareDomain)) {
	$bareDomain = 'gamepedia.com'; // Bare domain for this wiki farm.  Used to parameterize URLs in this file.
}

// Change $wgCacheEpoch when doing major deploys in where it would be helpful to essentially mass dump caches.
$wgCacheEpoch = '20191120211109';
$wgInvalidateCacheOnLocalSettingsChange = false;

// Default path, URL handling, and language.
$wgScriptPath		= "";
$wgArticlePath		= "/$1";
$wgUsePathInfo		= true;
$wgLanguageCode		= "en";

// Skin
$wgDefaultSkin = 'hydra';
$wgSkipSkins = ["chick", "cologneblue", "exvius", "hydradark", "minerva", "minervaneue", "modern", "monobook", "myskin", "netbar", "nostalgia", "simple", "standard", "vector"];
$wgVectorResponsive = true;

// Rights, Licensing
$wgRightsPage				= "Site License"; // Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl				= "//creativecommons.org/licenses/by-nc-sa/3.0/";
$wgRightsText				= "CC BY-NC-SA 3.0";
$wgRightsIcon				= "//hydra-media.cursecdn.com/commons.{$bareDomain}/b/b8/Attribution-NonCommercial-ShareAlike_CC_BY-NC-SA.png";

// Statsd server address and prefix
$wgStatsdServer = "localhost:8125";
$wgStatsdMetricPrefix = "hydra";

// Redis Servers.
$wgRedisServers = [
	'cache'		=> [
		'host'		=> 'redis-1_cluster.redis.local.curse.us',
		'port'		=> 6379,
		'options'	=> [
			'prefix'			=> 'Hydra:',
			'serializer'		=> 'none',
			'readTimeout'		=> 5
		]
	],
	// This is 'cache' without a prefix.
	'checkout'		=> [
		'host'		=> 'redis-1_cluster.redis.local.curse.us',
		'port'		=> 6379,
		'options'	=> [
			'prefix'			=> '',
			'serializer'		=> 'none',
			'readTimeout'		=> 5
		]
	],
	// This is 'cache' without a timeout.
	'worker'		=> [
		'host'		=> 'redis-1_cluster.redis.local.curse.us',
		'port'		=> 6379,
		'options'	=> [
			'prefix'		=> 'Hydra:',
			'serializer'	=> 'none',
			'readTimeout'	=> -1
		]
	],
	'session'	=> [
		'host'		=> 'redis-2_cluster.redis.local.curse.us',
		'port'		=> 6379,
		'options'	=> [
			'prefix'			=> 'hydra:',
			'serializer'		=> 'none'
		]
	],
	'jobqueue'		=> [
		'host'		=> 'redis-3_cluster.redis.local.curse.us',
		'port'		=> 6379,
		'options'	=> [
			'serializer'	=> 'none'
		]
	]
];

/**
 * MW Redis Job Queue
 */
$wgJobRunRate = 0;
// On beta checkout nodes, the MediaWiki jobqueue must use a different redis server to keep the two checkouts' jobs isolated.
$jobqueueRedisServer = $wgRedisServers['jobqueue'];
if (isset($_SERVER['HYDRA_VERSION']) && $_SERVER['HYDRA_VERSION'] == 'beta') {
	$jobqueueRedisServer = $wgRedisServers['worker'];
}
$wgJobTypeConf['default'] = [
	'class'			=> 'JobQueueRedis',
	'redisServer'	=> $jobqueueRedisServer['host'] . ":" . $jobqueueRedisServer['port'],
	'redisConfig'	=> $jobqueueRedisServer['options'],
	'claimTTL'		=> 3600,
	'daemonized'	=> true
];
$wgJobQueueAggregator = [
	'class'			=> 'JobQueueAggregatorRedis',
	'redisServer'	=> $jobqueueRedisServer['host'] . ":" . $jobqueueRedisServer['port'],
	'redisConfig'	=> $jobqueueRedisServer['options'],
];

// Need to set a session name or each wiki will use its own of $wgCookiePrefix.'_session'.
// Local sessions are per wiki while the network_session cookie handled by Extension:HydraAuth handles cross wiki sessions to trigger MediaWiki to make new local_session cookies.
$wgSessionName			= 'local_session';
$wgObjectCacheSessionExpiry = $wgCookieExpiration;

// Database configuration for LBFactoryMulti
$wgLBFactoryConf = [
	'class' => 'LBFactoryMulti',
	'serverTemplate' => [
		'user'     => 'wiki',
		'password' => $secrets['CHILD_WIKI_PASSWORD'],
		'type'     => 'mysql',
		'driver'   => 'aurora'
	],
	'templateOverridesBySection' => [
		'master' => [
			'user'     => 'master',
			'password' => $secrets['MASTER_WIKI_PASSWORD']
		],
		'commons' => [
			'user'     => 'commons',
			'password' => $secrets['COMMONS_WIKI_PASSWORD']
		],
	],
	'hostsByName' => [
		'masterdbw'  => 'masterdb_cluster.aurora.local.curse.us:3306',
		'masterdbr'  => 'masterdb-slave.aurora.local.curse.us:3306',
		'commonsdbw' => 'commonsdb_cluster.aurora.local.curse.us:3306',
		'commonsdbr' => 'commonsdb-slave.aurora.local.curse.us:3306',
		'db1w'       => 'db1_cluster.aurora.local.curse.us:3306',
		'db1r'       => 'db1-slave.aurora.local.curse.us:3306',
		'db2w'       => 'db2_cluster.aurora.local.curse.us:3306',
		'db2r'       => 'db2-slave.aurora.local.curse.us:3306',
		'db3w'       => 'db3_cluster.aurora.local.curse.us:3306',
		'db3r'       => 'db3-slave.aurora.local.curse.us:3306',
		'smw1w'      => 'smw1_cluster.aurora.local.curse.us:3306',
		'smw1r'      => 'smw1-slave.aurora.local.curse.us:3306',
		'esports1w'  => 'esports1_cluster.aurora.local.curse.us:3306',
		'esports1r'  => 'esports1-slave.aurora.local.curse.us:3306',
	],
	'sectionLoads' => [
		'master' => [
			'masterdbw' => 0,
			'masterdbr' => 1
		],
		'commons' => [
			'commonsdbw' => 0,
			'commonsdbr' => 1
		],
		'db1' => [
			'db1w' => 0,
			'db1r' => 1
		],
		'db2' => [
			'db2w' => 0,
			'db2r' => 1
		],
		'db3' => [
			'db3w' => 0,
			'db3r' => 1
		],
		'smw1' => [
			'smw1w' => 0,
			'smw1r' => 1
		],
		'esports1' => [
			'esports1w' => 0,
			'esports1r' => 1
		]
	],
	'sectionsByDB' => [
		'hydra' => 'master',
		'commons_hydra' => 'commons',
	]
];
// Backwards compatibility: allow either commonsdb or commons and masterdb or master
// as both section names and external cluster names.
$wgLBFactoryConf['sectionLoads']['commonsdb']               = $wgLBFactoryConf['sectionLoads']['commons'];
$wgLBFactoryConf['sectionLoads']['masterdb']                = $wgLBFactoryConf['sectionLoads']['master'];
$wgLBFactoryConf['templateOverridesBySection']['commonsdb'] = $wgLBFactoryConf['templateOverridesBySection']['commons'];
$wgLBFactoryConf['templateOverridesBySection']['masterdb']  = $wgLBFactoryConf['templateOverridesBySection']['master'];
// Mirror section config into external clusters since we have code that uses both.
$wgLBFactoryConf['externalLoads']              = $wgLBFactoryConf['sectionLoads'];
$wgLBFactoryConf['templateOverridesByCluster'] = $wgLBFactoryConf['templateOverridesBySection'];

// MySQL specific settings
$wgDBtype				= 'mysql';
$wgDSDBMySQLDriver		= 'aurora';
$wgDBprefix				= '';
$wgDBmysql5				= true; // 2019-03 - This setting is near being deprecated.  Only used in a couple remaining places.
$wgDBTableOptions		= "ENGINE=InnoDB, DEFAULT CHARSET=binary";
// This gets morphed into a regular server for the master wiki in master/LocalSettings.php.
$wgExternalServers	= [
	'master'	=> [
		[
			'host'		=> 'masterdb_cluster.aurora.local.curse.us:3306',
			'dbname'	=> 'hydra',
			'user'		=> 'master',
			'password'	=> $secrets['MASTER_WIKI_PASSWORD'],
			'type'		=> $wgDBtype,
			'driver'	=> 'aurora',
			'load'		=> 1
		],
		[
			'host'		=> 'masterdb-slave.aurora.local.curse.us:3306',
			'dbname'	=> 'hydra',
			'user'		=> 'master',
			'password'	=> $secrets['MASTER_WIKI_PASSWORD'],
			'type'		=> $wgDBtype,
			'driver'	=> 'aurora',
			'load'		=> 1
		]
	]
];
$wgMasterDatabaseName = $wgExternalServers['master'][0]['dbname'];

$wgBotPasswordsCluster = 'master';
$wgBotPasswordsDatabase = 'hydra';

// Shared memory settings
$wgMemCachedServers = [
	"default-memcache_1.memcache.local.curse.us",
	"default-memcache_2.memcache.local.curse.us",
	"default-memcache_3.memcache.local.curse.us"
];
if (isset($_SERVER['MEMCACHED_SERVERS'])) {
	$wgMemCachedServers = explode(';', $_SERVER['MEMCACHED_SERVERS']);
}
$wgObjectCaches['memcached-pecl'] = [
	'class'					=> 'MemcachedPeclBagOStuff',
	'loggroup'				=> 'memcached',
	'persistent'			=> false,
	'connect_timeout'		=> 1,
	'timeout'				=> 50000,
	'serializer'			=> 'igbinary',
	'use_binary_protocol'	=> true,
	'servers'				=> $wgMemCachedServers
];
$wgObjectCaches['memcached-pecl-local'] = [
	'class'					=> 'MemcachedPeclBagOStuff',
	'loggroup'				=> 'memcached',
	'persistent'			=> false,
	'connect_timeout'		=> 1,
	'timeout'				=> 50000,
	'serializer'			=> 'igbinary',
	'use_binary_protocol'	=> true,
	'servers'				=> ["memcached"]
];
$wgObjectCaches['redis-php'] = [
	'class'		=> 'RedisBagOStuff',
	'loggroup'	=> 'redis',
	'servers'	=> [
		$wgRedisServers['session']['host'] . ":" . $wgRedisServers['session']['port']
	]
];
$wgMainCacheType			= 'memcached-pecl';
// Configure the WAN cache explicitly, since MediaWiki is failing to do so early enough in Setup.php
$wgMainWANCache = 'memcached-pecl-wan';
$wgWANObjectCaches['memcached-pecl-wan'] = [
	'class' => 'WANObjectCache',
	'cacheId' => 'memcached-pecl'
];
$wgParserCacheType			= 'memcached-pecl';
$wgMessageCacheType			= 'memcached-pecl';
$wgUseLocalMessageCache		= true;
$wgSessionCacheType			= 'redis-php';
$wgMainStash				= 'memcached-pecl';
$wgFileStatCacheType		= 'memcached-pecl';

// Email
$wgEnableEmail			= true;
$wgEnableUserEmail		= true;
$wgUserEmailUseReplyTo	= true;
$wgEmergencyContact		= "help@{$bareDomain}";
$wgPasswordSender		= "help@{$bareDomain}";
$wgNoReplyAddress		= "noreply@{$bareDomain}";
$wgPasswordSenderName	= "Wiki Farm";
$wgSMTP = [
	'host'   	=> "localhost",
	'IDHost' 	=> $bareDomain,
	'port'   	=> 25
];
$wgAllowHTMLEmail		= true;
$wgEnotifUserTalk		= true;
$wgEnotifWatchlist		= true;
$wgEmailAuthentication	= true;

// Required for account creation and HydraAuth.  DO NOT REMOVE OR CHANGE!  Email validation is handled by Mercury.
$wgEmailConfirmToEdit	= false;
$wgEmailRequiredToRegister = true;

// Everything else.
$wgAccountCreationThrottle            = 7;
$wgAllowChunkedUploads                = true;
$wgAllowSiteCSSOnRestrictedPages      = true;
$wgAllowSpecialInclusion              = false;
$wgAllowTitlesInSVG                   = true;
$wgAllowUserCss                       = true;
$wgAllowUserJs                        = true;
$wgAuthenticationTokenVersion         = 1;
$wgBreakFrames                        = true;
$wgCookieSecure                       = true;
$wgDefaultUserOptions['prefershttps'] = true;
$wgDisableCounters                    = true;
$wgEnableCanonicalServerLink          = true;
$wgEnableMetaDescriptionFunctions     = true;
$wgEnableSphinxPrefixSearch           = true;
$wgEnableUploads                      = true;
$wgExtraSignatureNamespaces           = [NS_PROJECT, NS_MAIN];
$wgFileExtensions[]                   = 'bmp';
$wgFileExtensions[]                   = 'gif';
$wgFileExtensions[]                   = 'ico';
$wgFileExtensions[]                   = 'jpeg';
$wgFileExtensions[]                   = 'jpg';
$wgFileExtensions[]                   = 'mp3';
$wgFileExtensions[]                   = 'ogg';
$wgFileExtensions[]                   = 'pdf';
$wgFileExtensions[]                   = 'png';
$wgFileExtensions[]                   = 'sc2map';
$wgFileExtensions[]                   = 'svg';
$wgFileExtensions[]                   = 'webp';
$wgFooterIcons['poweredby']['hydra']  = [
	'src' => $wgScriptPath . '/skins/Hydra/images/icons/poweredbyhydra.png',
	'url' => "https://help.{$bareDomain}/What_is_Hydra",
	'alt' => 'Powered by Hydra',
];
$wgIllegalFileChars                   = ":\"+\/;?*\\<>";
$wgImageMagickConvertCommand          = "/usr/bin/convert";
$wgMaxImageArea                       = 25000000; // 5,000 x 5,000
$wgMaxAnimatedGifArea                 = $wgMaxImageArea; // This needs to come after $wgMaxImageArea obviously.
$wgMaxShellFileSize                   = 1024000;
$wgMaxShellMemory                     = 819200;
$wgMinervaEnableSiteNotice            = true;
$wgNamespacesWithSubpages             = array_fill(0, 200, true);
$wgResourceLoaderMaxQueryLength       = 2000; // Set this explicitly to avoid environment-specific weirdness
$wgShellLocale                        = "C.UTF-8"; // Even though this matches the default we leave this here to avoid issues if we miss it changing during an upgrade.
$wgShowExceptionDetails               = true;
$wgUseAjax                            = true;
$wgUseImageMagick                     = true;
$wgUseInstantCommons                  = false;
$wgUseTeX                             = false;
$wgSVGMetadataCutoff                  = 1024000;
$wgNoFollowDomainExceptions           = [
	'abiosgaming.com',
	'bukkit.org',
	'curse.com',
	'curseforge.com',
	'darkspyro.net',
	'diablofans.com',
	'fandom.com',
	'fm-base.co.uk',
	'futhead.com',
	'gamepedia.com',
	'hearthpwn.com',
	'infinityguru.com',
	'marriland.com',
	'minecraft.fr',
	'minecraftforum.net',
	'mmo-champion.com',
	'mtgsalvation.com',
	'muthead.com',
	'pathofpoe.com',
	'sc2mapster.com',
	'scrollsfans.com',
	'skyrimforge.com',
	'watchofyokai.com',
	'watchofyoukai.com',
	'wikia.com',
	'wikia.org',
	'wowace.com',
	'wowdb.com',
	'wowpedia.org'
];

// Parsoid
$wgVirtualRestConfig['modules']['parsoid'] = [
	'url'		=> 'http://parsoid.local.curse.us:80',
	'timeout'	=> 5
];

/**
 * Secrets
 */
// If these are changed we will not be able to decrypt important information and be royally screwed.
$wgSecretKey	= $secrets['WG_SECRET_KEY'];
$wgUpgradeKey	= $secrets['WG_UPGRADE_KEY'];

/**
 * Default Site Information
 */
$wgSitename			= "Hydra";
$wgMetaNamespace	= "Hydra";
$wgServer			= "https://hydra.{$bareDomain}";

/**
 * Media and CDN Settings
 */
$wgHydraMediaBasePath	= $IP . "/media";
$wgHydraCacheBasePath = "/media/hydra-cache";

/**
 * Hydra Commons
 */
if (isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'commons.gamepedia') === false) {
	$wgForeignFileRepos[] = [
		'class' => 'Fandom\\Includes\\Vignette\\VignetteAwareForeignDBViaLBRepo',
		'name' => 'Hydra_Commons',
		'url' => 'https://static.wikia.nocookie.net/commons_hydra/images',
		'hashLevels' => 2,
		'wiki' => 'commons_hydra',
		'hasSharedCache' => false,
		'descBaseUrl' => 'https://commons.gamepedia.com/File:',
		'fetchDescription' => false,
		'backend' => 'gcs-backend',
		'thumbScriptUrl' => false,
		'transformVia404' => true,
		'deletedHashLevels' => 3,
		'zones' => [
			'public' => [
			  'container' => 'commons_hydra',
			  'directory' => 'images',
			],
			'temp' => [
			  'container' => 'commons_hydra',
			  'directory' => 'images/temp',
			],
			'thumb' => [
			  'container' => 'commons_hydra',
			  'directory' => 'images/thumb',
			],
			'deleted' => [
			  'container' => 'commons_hydra',
			  'directory' => 'images/deleted',
			],
			'archive' => [
			  'container' => 'commons_hydra',
			  'directory' => 'images/archive',
			],
		],
	];
}

/**
 * Purge Queue settings
 */
$wgUseSquid = true;
$wgSquidServers = ['127.0.0.1:9722'];

/**
 * Logging settings
 */
// Make sure we're in a context that supports logging
if (interface_exists('Psr\Log\LoggerInterface')) {
	require_once $IP . "/settings/MonologConfig.php";
}

/**
 * Development/Staging Overrides
 */
if (isset($_SERVER['PHP_ENV'])) {
	if ($_SERVER['PHP_ENV'] == 'staging' || $_SERVER['PHP_ENV'] == 'development') {
		$wgDebugDumpSql = true;
		$wgMemCachedDebug = true;
		$wgShowDBErrorBacktrace = true;
		$wgShowDBErrorBacktrace = true;
		$wgShowExceptionDetails = true;
		$wgShowSQLErrors = true;
	}
	if ($_SERVER['PHP_ENV'] == 'development') {
		// Varnish has to be disabled or none of the debug features will work.
		$wgDebugToolbar = true;
		$wgSquidServers = [];
		$wgUseSquid = false;

		$wgVirtualRestConfig['modules']['parsoid'] = [
			'url'		=> 'http://parsoid-1.stg.hydra.aws-internal.curse.us:80',
			'timeout'	=> 5
		];
	}
}

// Group Permissions - Can be overridden by individual wikis.
$wgGroupPermissions['autoconfirmed']['suppressredirect'] = true;

/**
 * DynamicSettings Bootstrap
 */
require $IP . '/extensions/DynamicSettings/boot/DetectWiki.php';

$wgDSDefaultAWSRegion = 'us-east-1';
$wgDSDefaultS3Bucket = 'h7ktnb-us-east-1-hydra-media';
$wgDSDefaultCloudfrontId = '';
$wgDSDefaultCloudfrontDomain = 'gamepedia.cursecdn.com';

/**
 * More Dev/Staging Overrides
 */
if (!defined('LOCAL_DEV')) {
	$wgWidgetsCompileDir = sys_get_temp_dir() . "/widgets/{$dsHost}/compiled_templates/";
}

if (isset($_SERVER['PHP_ENV']) && $_SERVER['PHP_ENV'] == 'staging') {
	$wgDSDefaultS3Bucket = 'h7ktnc-us-east-1-hydra-media';
	$wgDSDefaultCloudfrontId = 'E68KQ92VIM6T3';
	$wgDSDefaultCloudfrontDomain = 'hydra-media-stg.cursecdn.com';
}

/**
 * always On Extensions
 */
if (!defined('SETTINGS_ONLY')) {
	require $IP . '/settings/Extensions.php';
}

// Namespaces to avoid indexing
$wgNamespaceRobotPolicies[NS_USER_TALK] = "noindex";
$wgNamespaceRobotPolicies[NS_USER]      = "noindex";

// Group Permissions - Can't be overridden by individual wikis.

$wgGroupPermissions['autopatrol']['autopatrol'] = true;

$wgGroupPermissions['bureaucrat']['abusefilter-modify']				= true;
$wgGroupPermissions['bureaucrat']['abusefilter-modify-restricted']	= true;
$wgGroupPermissions['bureaucrat']['abusefilter-view']				= true;
$wgGroupPermissions['bureaucrat']['deletelogentry']					= true;
$wgGroupPermissions['bureaucrat']['deleterevision']					= true;
$wgGroupPermissions['bureaucrat']['reupload-own']					= true;
$wgGroupPermissions['bureaucrat']['suppressrevision']				= false;
$wgGroupPermissions['bureaucrat']['userrights']						= false;
$wgGroupPermissions['bureaucrat']['wiki_claims']					= false;

$restrictedGroups = ['global_bureaucrat', 'global_sysop', 'grasp', 'hydra_admin', 'hydra_staff', 'checkuser', 'global_bot', 'wiki_guardian'];
$wgAddGroups['bureaucrat'] = array_diff(array_keys($wgGroupPermissions), $restrictedGroups);
$wgRemoveGroups['bureaucrat'] = array_diff(array_keys($wgGroupPermissions), $restrictedGroups);

$wgGroupPermissions['sysop']['abusefilter-log']					= true;
$wgGroupPermissions['sysop']['abusefilter-log-detail']			= true;
$wgGroupPermissions['sysop']['abusefilter-modify']				= true;
$wgGroupPermissions['sysop']['abusefilter-modify-restricted']	= true;
$wgGroupPermissions['sysop']['abusefilter-private']				= true;
$wgGroupPermissions['sysop']['abusefilter-revert']				= true;
$wgGroupPermissions['sysop']['abusefilter-view']				= true;
$wgGroupPermissions['sysop']['globalblock']						= true;
$wgGroupPermissions['sysop']['interwiki']						= true;
$wgGroupPermissions['sysop']['mergehistory']					= true;
$wgGroupPermissions['sysop']['pagetranslation']					= true;
$wgGroupPermissions['sysop']['reupload-own']					= true;
$wgGroupPermissions['sysop']['suppressrevision']				= false;
$wgGroupPermissions['sysop']['translate']						= true;
$wgGroupPermissions['sysop']['translate-groupreview']			= true;
$wgGroupPermissions['sysop']['translate-import']				= true;
$wgGroupPermissions['sysop']['translate-manage']				= true;
$wgGroupPermissions['sysop']['translate-messagereview']			= true;
$wgAddGroups['sysop'][] = 'autopatrol';
$wgRemoveGroups['sysop'][] = 'autopatrol';

if (!isset($wgGroupPermissions['global_bureaucrat'])) {
	$wgGroupPermissions['global_bureaucrat'] = $wgGroupPermissions['bureaucrat'];
	$wgGroupPermissions['global_bureaucrat']['userrights'] = true;
}

if (!isset($wgGroupPermissions['global_sysop'])) {
	$wgGroupPermissions['global_sysop'] = $wgGroupPermissions['sysop'];
}

if (!isset($wgGroupPermissions['global_bot'])) {
	$wgGroupPermissions['global_bot'] = $wgGroupPermissions['bot'];
	$wgGroupPermissions['global_bot']['noratelimit'] = true;
}

$wgGroupPermissions['global_sysop']['checkuser']				= true;
$wgGroupPermissions['global_sysop']['checkuser-log']			= true;
$wgGroupPermissions['global_sysop']['deletelogentry']			= true;
$wgGroupPermissions['global_sysop']['deleterevision']			= true;
$wgGroupPermissions['global_sysop']['hideuser']					= true;
$wgGroupPermissions['global_sysop']['skincategories']			= true;
$wgGroupPermissions['global_sysop']['suppressrevision']			= true;
$wgGroupPermissions['global_sysop']['suppressionlog']			= true;
$wgGroupPermissions['global_sysop']['tboverride'] 				= true;
$wgGroupPermissions['global_sysop']['abusefilter-hide-log']		= true;
$wgGroupPermissions['global_sysop']['abusefilter-hidden-log']	= true;
$wgGroupPermissions['global_sysop']['abusefilter-private-log']	= true;

$wgGroupPermissions['hydra_staff']['wiki_claims'] = true;
if (isset($wgGroupPermissions['interface-admin'])) {
	$wgGroupPermissions['global_sysop'] += $wgGroupPermissions['interface-admin'];
	$wgGroupPermissions['hydra_staff'] += $wgGroupPermissions['interface-admin'];
	$wgGroupPermissions['sysop'] += $wgGroupPermissions['interface-admin'];
}
