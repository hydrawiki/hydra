<?php
if (extension_loaded('newrelic')) {
	newrelic_ignore_transaction();
}
define('SETTINGS_ONLY', true);
define('DBO_DEFAULT', 16);

$host = trim($_SERVER['HTTP_HOST']);
$check = parse_url('http://'.$host.'/',  PHP_URL_HOST);
$settingsFile = __DIR__.'/sites/'.$host.'/LocalSettings.php';
if ($host === $check && file_exists($settingsFile)) {
	ob_start();
	require($settingsFile);
	ob_end_clean();
	$fileUrl = null;
	$uri = $_GET['path'];
	$parts = explode('/', trim($uri, '/'));

	if (isset($parts[0])) {
		if ($parts[0] === 'hydra') {
			$host = parse_url($wgLocalFileRepo['url'], PHP_URL_HOST);
			$wgLocalFileRepo['url'] = 'https://'.$host.'/hydra';
			array_shift($parts);
		}
		if (strpos($parts[0], '.') !== false) {
			array_shift($parts);
		}
		$uri = '/'.implode('/', $parts);
	}

	if (isset($wgLocalFileRepo['backend']) && strpos($wgLocalFileRepo['backend'], 's3') === 0) {
		if (isset($_GET['version']) && !empty($_GET['version'])) {
			$uri .= '?version='.$_GET['version'];
		}
	}
	if ($wgLocalFileRepo['backend'] === 's3-gcs-migration' || (!empty($wgUploadBucket) && empty($wgLocalFileRepo))) {
		if (isset($_GET['version']) && !empty($_GET['version'])) {
			$uri .= '/revision/latest?cb='.$_GET['version'];
		}
		//This is a work around since SETTINGS_ONLY blocks Extension.php loading to get the correct information here.
		$wgLocalFileRepo['url'] = "https://static.wikia.nocookie.net/{$wgUploadBucket}/images";
	}
	$fileUrl = $wgLocalFileRepo['url'].$uri;

	if ($fileUrl !== null && $fileUrl !== $wgLocalFileRepo['url']) {
		http_response_code(302);
		header('Cache-Control: public, max-age=14400');
		header('Location: '.$fileUrl);
		exit;
	}
}
http_response_code(404);
