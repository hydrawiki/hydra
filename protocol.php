<?php
require __DIR__ . '/includes/WebStart.php';

echo "Request of getProtocol()...\n";
var_dump($wgRequest->getProtocol());
echo "\nContents of \$_SERVER['HTTPS']...\n";
var_dump($_SERVER['HTTPS']);
echo "\nContents of \$_SERVER['HTTP_X_FORWARDED_PROTO']...\n";
var_dump($_SERVER['HTTP_X_FORWARDED_PROTO']);
