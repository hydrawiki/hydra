<?php
/**
 *********************************************************
 * HYDRA DEVELOPERS - PLEASE READ
 *********************************************************
 *
 * Set up custom logging for MediaWiki
 */

$wgMWLoggerDefaultSpi = [
	'class' => \MediaWiki\Logger\MonologSpi::class,
	'args' => [ [
		'loggers' => [
			'@default' => [
				'processors' => ['wiki', 'psr', 'web'],
				'handlers'   => ['syslog-warning']
			],
			// DBPerformance is noisy on our stack
			'DBPerformance' => [
				'handlers' => ['null']
			],
			// DBQuery warnings are excessive with SMW (HYD-4753)
			'DBQuery' => [
				'processors' => ['wiki', 'psr', 'web'],
				'handlers' => ['syslog-error']
			],
			// error-json duplicates error and ignores warning suppression
			'error-json' => [
				'handlers' => ['null']
			],
			// memcached needs to be suppressed until HYD-4749 is addressed
			'memcached' => [
				'handlers' => ['null']
			],
		],
		'processors' => [
			// 'wiki' adds some Mediawiki information to the context:
			//   wfHostname(), wfWikiID(), $wgVersion, \WebRequest::getRequestId()
			'wiki' => [
				'class' => \MediaWiki\Logger\Monolog\WikiProcessor::class,
			],
			// 'psr' substitutes the log message with values from the context:
			'psr' => [
				'class' => \Monolog\Processor\PsrLogMessageProcessor::class,
			],
			// 'web' adds web request information to the context:
			//   URL, client IP, GET/POST, server name, referer
			'web' => [
				'class' => \Monolog\Processor\WebProcessor::class,
			],
		],
		'handlers' => [
			// null can be used to /dev/null spammy (non-debug) channels
			'null' => [
				'class' => \Monolog\Handler\NullHandler::class
			],
			// syslog for all messages
			'syslog-debug' => [
				'class' => \Monolog\Handler\SyslogUdpHandler::class,
				// this listener is configured in .config/syslog-ng/hydra.conf
				'args' => ['127.0.0.1', 24403],
				'formatter' => 'json'
			],
			// syslog for warning+ messages
			'syslog-warning' => [
				'class' => \Monolog\Handler\SyslogUdpHandler::class,
				// this listener is configured in .config/syslog-ng/hydra.conf
				'args' => ['127.0.0.1', 24403, LOG_USER, \Monolog\Logger::WARNING],
				'formatter' => 'json'
			],
			// syslog for error+ messages
			'syslog-error' => [
				'class' => \Monolog\Handler\FilterHandler::class,
				'args' => [
					function () {
						return \MediaWiki\Logger\LoggerFactory::getProvider()->getHandler(
							'syslog-warning'
						);
					}, // $handler
					\Monolog\Logger::ERROR // $minLevelOrList
				]
			],
			// this handler sends all debug messages when a request has a warning or higher message
			// note that this will make time appear to jump backwards when it is triggered
			'fingers-crossed-syslog-debug' => [
				'class' => \Monolog\Handler\FingersCrossedHandler::class,
				'args' => [
					function () {
						// This closure argument is invoked by ObjectFactory to get the handler:
						return \MediaWiki\Logger\LoggerFactory::getProvider()->getHandler('syslog-debug');
					}, // $handler
					null, // $activationStrategy
					16384 // $bufferSize
				]
			],
			// this is a wrapper for fingers-crossed-syslog-debug which only allows INFO or above
			'fingers-crossed-syslog-info' => [
				'class' => \Monolog\Handler\FilterHandler::class,
				'args' => [
					function () {
						return \MediaWiki\Logger\LoggerFactory::getProvider()->getHandler(
							'fingers-crossed-syslog-debug'
						);
					}, // $handler
					\Monolog\Logger::INFO // $minLevelOrList
				]
			],
		],
		'formatters' => [
			'json' => [
				'class' => \Monolog\Formatter\JsonFormatter::class,
				// separate batched records by newlines
				'args' => [\Monolog\Formatter\JsonFormatter::BATCH_MODE_NEWLINES]
			],
		]
	]
	]
];
