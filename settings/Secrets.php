<?php
/**
 *********************************************************
 * HYDRA DEVELOPERS - PLEASE READ
 *********************************************************
 *
 * Secrets handles loading secret environment variables that are used
 * to bootstrap the hydra platform. All required secret values are
 * initialized here as empty strings to provide the schema.
 */

/**
 * Container for system secrets
 *
 * @var array $secrets = [
 *    'HYDRA_BARE_DOMAIN'      => (string) The absolutely bare naked domain of the farm.
 *    'MASTER_WIKI_PASSWORD'   => (string) The password the master wiki database where centralized wiki configuration,
 *                                         abuse filter, global block, and other things occur.
 *    'COMMONS_WIKI_PASSWORD'  => (string) The password for the commons wiki database.
 *    'CHILD_WIKI_PASSWORD'    => (string) The password for the child wiki databases.
 *    'WG_SECRET_KEY'          => (string) MediaWiki $wgSecretKey
 *    'WG_UPGRADE_KEY          => (string) MediaWiki $wgSecretKey
 *    'DS_API_SERVER_TOKEN'    => (string) $wgDSAPIServerToken
 *    'DS_INSTALL_DB_PASSWORD' => (string) $wgDSInstallDBUser['password']
 *    'DS_HTTP_BASIC_AUTH      => (string) $wgDSHydraHttpAuthInfo - Used for getting in through HTTP Basic
 *                                         Authentication on hydra.gamepedia.com.
 *    'CHEEVOS_CLIENT_ID'      => (string) $wgCheevosClientId
 *    'MERCURY_API_KEY'        => (string) $wgMercuryAPIKey
 *    'MERCURY_CRYPT_KEY       => (string) $wgMercuryCryptKey
 *    'MERCURY_SESSION_KEY'    => (string) $wgMercurySessionKey
 *    'GP_PRO_API_KEY'         => (string) $wgGProApiConfig['api_key']
 *    'GPE_INSTALL_PASSWORD'   => (string) $wgDSGlobalPageEditorInstallAccount['password']
 *    'GPE_BOT_PASSWORD'       => (string) $wgDSGlobalPageEditorAccount['password']
 *    'REVERB_API_KEY'         => (string) $wgReverbApiKey
 *    'GCS_CREDENTIALS'        => (string) $wgGcsCredentials
 * ]
 */
$secrets = [
	'HYDRA_BARE_DOMAIN'      => '',
	'MASTER_WIKI_PASSWORD'   => '',
	'COMMONS_WIKI_PASSWORD'  => '',
	'CHILD_WIKI_PASSWORD'    => '',
	'WG_SECRET_KEY'          => '',
	'WG_UPGRADE_KEY'         => '',
	'DS_API_SERVER_TOKEN'    => '',
	'DS_INSTALL_DB_PASSWORD' => '',
	'DS_HTTP_BASIC_AUTH'     => '',
	'CHEEVOS_CLIENT_ID'      => '',
	'MERCURY_API_KEY'        => '',
	'MERCURY_CRYPT_KEY'      => '',
	'MERCURY_SESSION_KEY'    => '',
	'GP_PRO_API_KEY'         => '',
	'GPE_INSTALL_PASSWORD'   => '',
	'GPE_BOT_PASSWORD'       => '',
	'REVERB_API_KEY'         => '',
	'GCS_CREDENTIALS'        => ''
];

if (file_exists("{$IP}/.config/hydra_secrets.json")) {
	$rawJson = file_get_contents("{$IP}/.config/hydra_secrets.json");
	$json = json_decode($rawJson, true);
	if (is_array($json)) {
		$secrets = array_merge($secrets, $json);
	}
}

// Load variables from .env
if (function_exists("loadDotEnv")) {
	loadDotEnv();
}

foreach ($secrets as $secret => $value) {
	$fromEnv = getenv($secret, true);
	if ($fromEnv !== false) {
		$secrets[$secret] = $fromEnv;
	}
}
