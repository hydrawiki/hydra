<?php
/**
 *********************************************************
 * HYDRA DEVELOPERS - PLEASE READ
 *********************************************************
 *
 * Local Settings file for the master wiki
 */

// Database settings
$wgDBserver			= $wgExternalServers['master'][0]['host'];
$wgDBname			= $wgExternalServers['master'][0]['dbname'];
$wgDBuser			= $wgExternalServers['master'][0]['user'];
$wgDBpassword		= $wgExternalServers['master'][0]['password'];
$wgDBservers = [
	[
		'host' => $wgExternalServers['master'][0]['host'],
		'dbname' => $wgExternalServers['master'][0]['dbname'],
		'user' => $wgExternalServers['master'][0]['user'],
		'password' => $wgExternalServers['master'][0]['password'],
		'type' => 'mysql',
		'driver' => 'aurora',
		'flags' => DBO_DEFAULT,
		'load' => 0
	]
];

// Each cluster is an external server for shared user and actor tables
$wgExternalServers['commons'] = [
	[
		'host'		=> 'commonsdb_cluster.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'commons',
		'password'	=> $secrets['COMMONS_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 0
	],
	[
		'host'		=> 'commonsdb-slave.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'commons',
		'password'	=> $secrets['COMMONS_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 1
	]
];
$wgExternalServers['db1'] = [
	[
		'host'		=> 'db1_cluster.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 0
	],
	[
		'host'		=> 'db1-slave.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 1
	]
];
$wgExternalServers['db2'] = [
	[
		'host'		=> 'db2_cluster.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 0
	],
	[
		'host'		=> 'db2-slave.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 1
	]
];
$wgExternalServers['db3'] = [
	[
		'host'		=> 'db3_cluster.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 0
	],
	[
		'host'		=> 'db3-slave.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 1
	]
];
$wgExternalServers['smw1'] = [
	[
		'host'		=> 'smw1_cluster.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 0
	],
	[
		'host'		=> 'smw1-slave.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 1
	]
];
$wgExternalServers['esports1'] = [
	[
		'host'		=> 'esports1_cluster.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 0
	],
	[
		'host'		=> 'esports1-slave.aurora.local.curse.us:3306',
		'dbname'	=> 'hydra',
		'user'		=> 'wiki',
		'password'	=> $secrets['CHILD_WIKI_PASSWORD'],
		'type'		=> $wgDBtype,
		'driver'	=> 'aurora',
		'load'		=> 1
	]
];

// This is ordered in this way so that it shows up in a specific when it is array_reverse()'ed in the database selector.
$wgChildClusterServers = [
	'commons',
	'smw1',
	'esports1',
	'db1',
	'db2',
	'db3'
];

$wgSharedDB = 'hydra';
$wgSharedTables = ['user', 'user_global', 'actor'];

// Set $wgCacheDirectory to a writable directory on the web server
// to make your wiki go slightly faster. The directory should not
// be publicly accessible from the web.

$wgCacheDirectory    = "{$wgHydraCacheBasePath}/{$dsHost}";
$wgUploadDirectory   = "{$wgHydraMediaBasePath}/{$dsHost}";

$wgLocalInterwiki = strtolower($wgSitename);

$bodyName = "Hydra";

$wgRightsUrl = null;
$wgRightsText = "All Rights Reserved";
$wgRightsIcon = null;

$wgCrossSiteAJAXdomains[] = '*.' . $bareDomain;

$wgUploadBucket = 'hydra';

$socialSettings = [
	'sub_reddit'			=> '',
	'hash_tags'				=> '',
	'twitter_via'			=> 'CurseGamepedia',
	'prompt_cookie_time'	=> 3600, // One Hour
	'sidebar_enabled'		=> true,
	'navigation_enabled'	=> true,
	'prompts_enabled'		=> true,
	'sidebar_image_set'		=> 'square_icons',
	'share_image_set'		=> 'square_icons',
	'profile_urls'			=> [
		'twitter'		=> 'https://twitter.com/CurseGamepedia',
		'facebook'		=> 'https://www.facebook.com/CurseGamepedia'
	]
];

$wgUploadPath = $wgLocalFileRepo['url'];

$wgLogo = "{$wgUploadPath}/b/bc/Wiki.png";
$wgFavicon = "{$wgUploadPath}/6/64/Favicon.ico";
$wgMobileFrontendLogo = "{$wgUploadPath}/0/00/Mobile_logo.png";

$wgDSMasterDomains = [
	"hydra.gamepedia.com",
	"hydra.gamepedia.io",
	"hydra.gamepedia.wiki"
];

$dsSiteKey = 'master';
define('MASTER_WIKI', true);

define("NS_DIFFS", 1000);
define("NS_DIFFS_TALK", 1001);

$wgExtraNamespaces[NS_DIFFS]				= "Diffs";
$wgExtraNamespaces[NS_DIFFS_TALK]			= "Diffs_talk";
$wgNamespaceProtection[NS_DIFFS]			= ['editdiffs'];
$wgNamespacesWithSubpages[NS_DIFFS]			= true;
$wgNamespacesWithSubpages[NS_DIFFS_TALK]	= true;

$wgGroupPermissions['bureaucrat']['userrights'] = false;

$wgGroupPermissions['hydra_admin'] = $wgGroupPermissions['sysop'];
$wgGroupPermissions['hydra_admin']['abusefilter-modify-global']		= true;
$wgGroupPermissions['hydra_admin']['editdiffs']						= true;
$wgGroupPermissions['hydra_admin']['edit_email_body']				= true;
$wgGroupPermissions['hydra_admin']['font_manager']					= true;
$wgGroupPermissions['hydra_admin']['font_upload']					= true;
$wgGroupPermissions['hydra_admin']['global_page_editor']			= true;
$wgGroupPermissions['hydra_admin']['lock_settings']					= true;
$wgGroupPermissions['hydra_admin']['maintenancerunner']				= true;
$wgGroupPermissions['hydra_admin']['profile-stats']					= true; // view stats on curseprofile adoption
$wgGroupPermissions['hydra_admin']['sites_edit_database']			= true;
$wgGroupPermissions['hydra_admin']['sites_edit_information']		= true;
$wgGroupPermissions['hydra_admin']['sites_edit_name_domain']		= true;
$wgGroupPermissions['hydra_admin']['sites_edit_search']				= true;
$wgGroupPermissions['hydra_admin']['sites_edit_type']				= true;
$wgGroupPermissions['hydra_admin']['subscription']					= true;
$wgGroupPermissions['hydra_admin']['userrights']					= true;
$wgGroupPermissions['hydra_admin']['user_engagement']				= true;
$wgGroupPermissions['hydra_admin']['user_rights_global']			= true;
$wgGroupPermissions['hydra_admin']['wiki_add_edit']					= true;
$wgGroupPermissions['hydra_admin']['wiki_add_new']					= true;
$wgGroupPermissions['hydra_admin']['wiki_advertisements']			= true;
$wgGroupPermissions['hydra_admin']['wiki_advertisements_defaults']	= true;
$wgGroupPermissions['hydra_admin']['wiki_allowed_extensions']		= true;
$wgGroupPermissions['hydra_admin']['wiki_allowed_settings']			= true;
$wgGroupPermissions['hydra_admin']['wiki_build_sphinx']				= true;
$wgGroupPermissions['hydra_admin']['wiki_delete']					= true;
$wgGroupPermissions['hydra_admin']['wiki_edit_log']					= true;
$wgGroupPermissions['hydra_admin']['wiki_edit_log_form_data']		= true;
$wgGroupPermissions['hydra_admin']['wiki_edit_log_hard_delete']		= true;
$wgGroupPermissions['hydra_admin']['wiki_edit_log_revert']			= true;
$wgGroupPermissions['hydra_admin']['wiki_edit_log_soft_delete']		= true;
$wgGroupPermissions['hydra_admin']['wiki_extensions']				= true;
$wgGroupPermissions['hydra_admin']['wiki_group_permissions']		= true;
$wgGroupPermissions['hydra_admin']['wiki_hostHelper']				= true;
$wgGroupPermissions['hydra_admin']['wiki_install']					= true;
$wgGroupPermissions['hydra_admin']['wiki_mass_action']				= true;
$wgGroupPermissions['hydra_admin']['wiki_manage']					= true;
$wgGroupPermissions['hydra_admin']['wiki_namespaces']				= true;
$wgGroupPermissions['hydra_admin']['wiki_points_multipliers']		= true;
$wgGroupPermissions['hydra_admin']['wiki_promotions']				= true;
$wgGroupPermissions['hydra_admin']['wiki_promotions_recache']		= true;
$wgGroupPermissions['hydra_admin']['wiki_rebuildLanguage']			= true;
$wgGroupPermissions['hydra_admin']['wiki_recache']					= true;
$wgGroupPermissions['hydra_admin']['wiki_recache_ads']				= true;
$wgGroupPermissions['hydra_admin']['wiki_scrape']					= true;
$wgGroupPermissions['hydra_admin']['wiki_searchReindex']			= true;
$wgGroupPermissions['hydra_admin']['wiki_settings']					= true;
$wgGroupPermissions['hydra_admin']['wiki_sites']					= true;
$wgGroupPermissions['hydra_admin']['wiki_tools_log']				= true;
$wgGroupPermissions['hydra_admin']['wiki_undelete']					= true;
$wgGroupPermissions['hydra_admin']['wiki_update']					= true;

$wgGroupPermissions['wiki_manager']['abusefilter-modify-global']	= true;
$wgGroupPermissions['wiki_manager']['sites_edit_information']		= true;
$wgGroupPermissions['wiki_manager']['sites_edit_name_domain']		= true;
$wgGroupPermissions['wiki_manager']['sites_edit_type']				= true;
$wgGroupPermissions['wiki_manager']['user_engagement']				= true;
$wgGroupPermissions['wiki_manager']['wiki_edit_log']				= true;
$wgGroupPermissions['wiki_manager']['wiki_extensions']				= true;
$wgGroupPermissions['wiki_manager']['wiki_manage']					= true;
$wgGroupPermissions['wiki_manager']['wiki_group_permissions']		= true;
$wgGroupPermissions['wiki_manager']['wiki_points_multipliers']		= true;
$wgGroupPermissions['wiki_manager']['wiki_searchReindex']			= true;
$wgGroupPermissions['wiki_manager']['wiki_settings']				= true;
$wgGroupPermissions['wiki_manager']['wiki_sites']					= true;

$wgGroupPermissions['ads_manager']['wiki_advertisements']			= true;
$wgGroupPermissions['ads_manager']['wiki_advertisements_defaults']	= true;
$wgGroupPermissions['ads_manager']['wiki_recache_ads']				= true;

$wgGroupPermissions['report_manager']['view_report_cards'] = true;

$wgGroupPermissions['promo_manager']['wiki_promotions']				= true;
$wgGroupPermissions['promo_manager']['wiki_promotions_recache']		= true;

if (!defined('SETTINGS_ONLY')) {
	wfLoadExtension('Elastica');
	require_once "{$IP}/extensions/CirrusSearch/CirrusSearch.php";
	if (PHP_SAPI != 'cli') {
		$wgSearchType = 'CirrusSearch';
	}

	wfLoadExtension('HydraCore');
	wfLoadExtension('SyncService');

	// Skins
	wfLoadSkin('Vector');
	wfLoadSkin('Hydra');
	wfLoadSkin('HydraDark');
	wfLoadSkin('MinervaNeue');
	wfLoadExtension('MobileFrontend');

	wfLoadExtension('AbuseFilter');
	$wgAbuseFilterIsCentral = true;
	wfLoadExtension('AllSites');
	wfLoadExtension('CacheBreaker');
	wfLoadExtension('CategoryTree');
	wfLoadExtension('CheckUser');
	wfLoadExtension('Cheevos');
	wfLoadExtension('Cite');
	wfLoadExtension('ClaimWiki');
	$wgClaimWikiEnabled = false;
	wfLoadExtension('CurseProfile');
	wfLoadExtension('CurseTwitter');
	wfLoadExtension('EmbedVideo');
	wfLoadExtension('GlobalBlock');
	$wgInstallGlobalBlockTables = true;
	wfLoadExtension('Helios');
	wfLoadExtension('InputBox');
	wfLoadExtension('Interwiki');
	wfLoadExtension('MobileFrontend');
	wfLoadExtension('MsUpload');
	wfLoadExtension('Nuke');
	wfLoadExtension('PageImages');
	wfLoadExtension('ParserFunctions');
	wfLoadExtension('Poem');
	wfLoadExtension('Popups');
	wfLoadExtension('Reverb');
	wfLoadExtension('SearchLogger');
	wfLoadExtension('SEO');
	wfLoadExtension('Social');
	wfLoadExtension('SpamBlacklist');
	wfLoadExtension('Subscription');
	$wgInstallSubscriptionTables = true;
	wfLoadExtension('TextExtracts');
	wfLoadExtension("ThanksMeToo");
	wfLoadExtension('TitleBlacklist');
	wfLoadExtension('Twiggy');
	wfLoadExtension('VisualEditor');
	wfLoadExtension('WikiEditor');

	wfLoadExtension('Hydralytics');
	wfLoadExtension('ShopVac');
	wfLoadExtension('SyntaxHighlight_GeSHi');
}

$wgCirrusSearchClusters = [
	'default' => [
		[
			'host' => 'elastic.local.curse.us',
			'port' => 80
		]
	]
];
$wgCirrusSearchReplicas = '0-1';
$wgCirrusSearchShardCount = ['content' => 3, 'general' => 3, 'archive' => 3, 'titlesuggest' => 3];

$wgPFEnableStringFunctions = true;

// Set for child wikis in Special:WikiAllowedSettings.
$wgRCWatchCategoryMembership = true;

$wgHydraSkinShowAnchorAd = false;
$wgHydraSkinShowFooterAd = false;
$wgHydraSkinShowSideRail = false;

$wgSiteMiscSlots['googleanalyticsid'] = 'UA-39967852-75'; // Do not add global roll ups here.

$wgUseTidy = true;
