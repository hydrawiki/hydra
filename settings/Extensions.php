<?php
/**
 *********************************************************
 * HYDRA DEVELOPERS - PLEASE READ
 *********************************************************
 *
 * Extensions in this file are considered so critical to the operation of Hydra that they must
 * not be defined in Special:WikiAllowedExtensions as to prevent them from being
 * accidentally turned off.
 */

// This can stay in this file forever.  We do not need to move it to allowed extensions.
wfLoadExtension('RedisCache');

wfLoadExtension('DynamicSettings');
// Security for API Server Call in Dynamic Settings
$wgDSAPIServerToken = $secrets['DS_API_SERVER_TOKEN'];
$wgDSInstallDBUser = [
	'user'		=> 'wiki-spinup',
	'password'	=> $secrets['DS_INSTALL_DB_PASSWORD']
];
$wgDSInstallDBNodeConfig = [
	'searchHost' => 'elastic.local.curse.us',
	'searchPort' => 80,
	'workerNode' => 'internal-hydra-worker.local.curse.us',
	'wikisPerNode' => 2000,
	'skipNodes' => ['master', 'commons']
];

if (isset($_SERVER['PHP_ENV']) && $_SERVER['PHP_ENV'] == 'development') {
	// Super Grant
	$wgDSDBUserGrants = [
		['host' => '10.20.%%.%%'],
		['host' => '10.101.%%.%%'],
	];
} else {
	// Web/Worker Nodes
	$wgDSDBUserGrants = [
		['host' => '10.125.%.%']
	];
}

$wgDSHydraHttpAuthInfo = $secrets['DS_HTTP_BASIC_AUTH'];
$wgDSGlobalPageEditorInstallAccount = [
	'user'		=> 'HydraGPEBot',
	'password'	=> $secrets['GPE_INSTALL_PASSWORD']
];

// Allows this to be overridden in LocalDevSettings.
if (!isset($wgDSGlobalPageEditorAccount)) {
	$wgDSGlobalPageEditorAccount = [
		'user'		=> 'HydraGPEBot@HydraGPEBot',
		'password'	=> $secrets['GPE_BOT_PASSWORD']
	];
}

$wgMercuryAuthDomain = "https://www.{$bareDomain}";

$wgMercuryAuthCookieDomain = ".{$bareDomain}";
$wgMercuryAPIUrl = "https://www.{$bareDomain}/api/";
$wgGProApiConfig = [
	"endpoint"		=> "//www.{$bareDomain}/",
	"https"			=> true,
	"ssl_verify"	=> true,
	"api_key"		=> $secrets['GP_PRO_API_KEY']
];
$wgMercuryAPIKey = $secrets['MERCURY_API_KEY'];
$wgMercuryCryptKey = $secrets['MERCURY_CRYPT_KEY'];
$wgMercurySessionKey = $secrets['MERCURY_SESSION_KEY'];

$wgReverbApiEndPoint = 'http://services.local.curse.us/reverb/v1';
$wgReverbApiKey = $secrets['REVERB_API_KEY'];

// @TODO: Remove Echo settings after second deploy.
$wgEchoCrossWikiNotifications = true;
// Work around for weird bug where $wgEchoCluster is not initialized in the updater.
$wgEchoCluster = false;
$wgEchoUnreadWikis = true;
$wgEchoSharedTrackingDB = 'hydra';
$wgEchoSharedTrackingCluster = 'master';
// Pretty sure we still need this as of MW 1.29.  Still a beta feature according to Echo.
$wgDefaultUserOptions['echo-cross-wiki-notifications'] = true;
$wgEchoUseJobQueue = true;

$wgCommunityManager = 'Heytots';
$wgHLFaqUrl = 'https://help.gamepedia.com/Policies,_procedures,_and_FAQ_(One-Stop_Shop)';
$wgHLFeedbackUrl = 'https://help.gamepedia.com/Talk:Analytics_Dashboard';
$wgHLDiscordUrl = 'https://help.gamepedia.com/Discord';
$wgStripGAProperties = [
	"UA-35871056-4",
	"UA-38793428-195"
];

$wgCheevosHost = 'http://achievements-service.local.curse.us/v2';
$wgCheevosClientId = $secrets['CHEEVOS_CLIENT_ID'];

$wgGlobalBlockingCluster = 'master';
$wgGlobalBlockingDatabase = 'hydra';

$wgAbuseFilterCentralCluster = 'master';
$wgAbuseFilterCentralDB = 'hydra';

$wgDSMasterDBLB = 'master';
$wgDSMasterDB = 'hydra';

// Whitelist the internal ip for AWS environment
$wgLinterSubmitterWhitelist = [
	'10.125.0.0/16' => true
];

wfLoadExtension('UserFixer');

wfLoadExtension('FandomGcs');
$wgGcsCredentials = $secrets['GCS_CREDENTIALS'];
$wgGcsConfig = [
	'gcsCredentials' => $wgGcsCredentials,
	'gcsBucket' => 'static-assets-originals'.(isset($_SERVER['PHP_ENV']) && ($_SERVER['PHP_ENV'] === 'staging' || $_SERVER['PHP_ENV'] === 'development') ? '-dev' : '-prod'),
	'gcsTemporaryBucket' => 'static-assets-temporary'.(isset($_SERVER['PHP_ENV']) && ($_SERVER['PHP_ENV'] === 'staging' || $_SERVER['PHP_ENV'] === 'development') ? '-dev' : '-prod')
];
$wgFileBackends['gcs-backend'] = [
	'name' => 'gcs-backend',
	'class' => 'Fandom\Includes\Gcs\GcsFileBackend',
	'lockManager' => 'nullLockManager',
	'wikiId'	=> '',
	'gcsCredentials' => $wgGcsConfig['gcsCredentials'],
	'gcsBucket' => $wgGcsConfig['gcsBucket'],
	'gcsTemporaryBucket' => $wgGcsConfig['gcsTemporaryBucket'],
	'gcsObjectNamePrefix' => 'mediawiki/',
];

if (!empty($wgUploadBucket)) {
	$wgUploadPath = "https://static.wikia.nocookie.net/{$wgUploadBucket}/images";
	$wgGenerateThumbnailOnParse = false;
	$wgLocalFileRepo = [
		'backend' => 'gcs-backend',
		'class' => 'Fandom\Includes\Vignette\VignetteAwareLocalRepo',
		'name' => 'local',
		'directory' => $wgUploadDirectory,
		'scriptDirUrl' => $wgScriptPath,
		'url' => $wgUploadPath,
		'hashLevels' => 2,
		'thumbScriptUrl' => false,
		'transformVia404' => true,
		'deletedDir' => $wgDeletedDirectory,
		'deletedHashLevels' => 3,
		'zones' => [
			'public' => [
				'container' => $wgUploadBucket,
				'directory' => 'images',
			],
			'temp' => [
				'container' => $wgUploadBucket,
				'directory' => 'images/temp',
			],
			'thumb' => [
				'container' => $wgUploadBucket,
				'directory' => 'images/thumb',
			],
			'deleted' => [
				'container' => $wgUploadBucket,
				'directory' => 'images/deleted',
			],
			'archive' => [
				'container' => $wgUploadBucket,
				'directory' => 'images/archive',
			],
		],
	];
	$wgMediaHandlers = [
		'image/jpeg' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteJpegHandler',
		'image/png' => '\Fandom\Includes\Vignette\MediaHandlers\VignettePngHandler',
		'image/gif' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteGifHandler',
		'image/tiff' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteTiffHandler',
		'image/webp' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteWebPHandler',
		'image/x-ms-bmp' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteBmpHandler',
		'image/x-bmp' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteBmpHandler',
		'image/x-xcf' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteXcfHandler',
		'image/svg+xml' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteSvgHandler',
		'image/svg' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteSvgHandler',
		'image/vnd.djvu' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteDjVuHandler',
		'image/x.djvu' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteDjVuHandler',
		'image/x-djvu' => '\Fandom\Includes\Vignette\MediaHandlers\VignetteDjVuHandler',
	];
	$wgMathFileBackend = 'gcs-backend';
	$wgTimelineFileBackend = 'gcs-backend';
}

/**
 * Custom Extensions
 */
if (is_file($IP . '/settings/LocalDevExtensions.php')) {
	require $IP . '/settings/LocalDevExtensions.php';
}
