<?php
/**
 * Hydra Helper File
 *
 ******************************************
 * HYDRA DEVELOPERS - PLEASE READ
 ******************************************
 * This file provides very basic functions that may need to be available
 * before the main MediaWiki system is fully operational. These functions
 * are loaded via composer and any downstream usage of these must be in
 * a tool chain that has required the vendor/autoload.php file. This file
 * should only have basic functions that have no dependency on MediaWiki,
 * and handle some development process or bootstrap step that can't be
 * performed via other MediaWiki Core systems.
 *
 */

if ( !function_exists( 'hdd' ) ) {
	/**
	 * Perform var_dump and die with pretty formating
	 *
	 * @return void
	 */
	function hdd() {
		array_map( function ( $x ) {
			dump( $x );
		}, func_get_args() );
		die;
	}
}

if ( !function_exists( 'loadDotEnv' ) ) {
	/**
	 * PhpDotEnv wrapper handles initializing DotEnv
	 * and loading a .env file if it exist.
	 *
	 * @return void
	 */
	function loadDotEnv() {
		$dotenv = Dotenv\Dotenv::create( __DIR__ );
		$dotenv->safeLoad();
	}
}

if ( !function_exists( 'env' ) ) {
	/**
	 * A getenv() wrapper that replicated the laravel env helper.
	 * It allows you to specify a default if no env value is found.
	 *
	 * @param string $key
	 * @param mixed|null $default
	 * @return mixed
	 */
	function env( $key, $default = null ) {
		return getenv( $key ) ?: $default;
	}
}

if ( !function_exists( 'is_countable' ) ) {
	/**
	 * Shim the is_countable method from php 7.3
	 *
	 * @param mixed $value
	 *
	 * @return bool
	 */
	function is_countable( $value ) {
		return is_array( $value ) || $value instanceof \Countable;
	}
}
